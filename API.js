import { 
    SERVER_ADDR
}from "./Config.js";

const call = async (url) => {
    const request = await fetch( SERVER_ADDR  + url);
    const result = await request.json();
    return result;
};

export const getAllcategories = async () => {
    let result = await call('products/categories')
    return result
};