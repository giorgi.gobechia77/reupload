
export const generateProduct = (product) => `
<div class="catalog__product">
<div class="catalog__photo">
<img src="${product.image}" />
</div>
<div class="catalog__name>
${product.name}
</div>
<div class="catalog__price">
${product.price}$
</div>
</div>
`;
export const generateProductHTML = (productList) => {
    let productHTML = '';
    for(const product of productList){
        productHTML += generateProduct(product)
    }
    return productHTML
};
